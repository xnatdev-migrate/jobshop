package org.nrg.scheduling.test;

import org.nrg.scheduling.MathUtils;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;
import static org.nrg.scheduling.test.ExampleJobs.*;

public class JobTests {

    @Test
    public void testTimes() {
        shortJob.setTimes(100);
        reallyLongJob.setTimes(100);

        assertTrue(MathUtils.doublesEqual(100, shortJob.getLaunchTime()));
        assertTrue(MathUtils.doublesEqual(200, shortJob.getEndTime()));
        assertTrue(MathUtils.doublesEqual(20100, reallyLongJob.getEndTime()));
    }

}
