package org.nrg.scheduling.test;

import org.nrg.scheduling.*;
import org.testng.annotations.Test;

import java.util.List;

import static org.nrg.scheduling.test.ExampleJobs.*;
import static org.testng.AssertJUnit.*;

public class HochbaumShmoysSolutionTests {

    @Test
    public void solutionTestA() {
        final ProcessorSchedulingProblem<String> problem = problemA();
        final ProcessorSchedulingSolution<String> solution = problem.solve(Algorithms.HOCHBAUM_SHMOYS);
        final Job<String> fs = solution.findJob("FS", problem.getJobList());
        final Job<String> manualPup = solution.findJob("Manual PUP", problem.getJobList());
        final Job<String> fsPup = solution.findJob("FS PUP", problem.getJobList());
        final Job<String> fBIRN = solution.findJob("FBIRN", problem.getJobList());
        final Job<String> bold = solution.findJob("BOLD", problem.getJobList());
        final Job<String> registerMR = solution.findJob("registerMR", problem.getJobList());
        final Job<String> wmh = solution.findJob("WMH", problem.getJobList());

        final List<JobList<String>> solutionMachineAllocation = solution.getSolutionByMachineAllocation();
        final int solvedNumMachines = solutionMachineAllocation.size();
        final double solvedMakespan = solution.getMakespan();
        final JobList<String> solvedMachine1 = solutionMachineAllocation.get(0);
        final JobList<String> solvedMachine2 = solutionMachineAllocation.get(1);
        final JobList<String> solvedMachine3 = solutionMachineAllocation.get(2);
        final JobList<String> solvedMachine4 = solutionMachineAllocation.get(3);

        assertTrue(MathUtils.doublesEqual(solvedMakespan, 840));
        assertEquals(4, solvedNumMachines);
        assertEquals(JobList.singleton(fs), solvedMachine1);
        assertEquals(JobList.singleton(fsPup), solvedMachine2);
        final JobList<String> remainingMachine1 = new JobList<>(manualPup, registerMR);
        final JobList<String> remainingMachine2 = new JobList<>(bold, wmh, fBIRN);
        if (solvedMachine3.contains(manualPup)) {
            assertEquals(remainingMachine1, solvedMachine3);
            assertEquals(remainingMachine2, solvedMachine4);
        } else {
            assertEquals(remainingMachine1, solvedMachine4);
            assertEquals(remainingMachine2, solvedMachine3);
        }

        assertEquals(new JobList<>(bold, wmh, manualPup, fBIRN, registerMR, fsPup, fs), solution.getSolutionByCompletionOrder());
    }

    @Test
    public void solutionTestB() {
        final ProcessorSchedulingSolution<Integer> solution = problemB().solve(Algorithms.HOCHBAUM_SHMOYS);
        assertTrue(MathUtils.doublesEqual(problemB().getJobList().get(0).getJobLength(), solution.getMakespan()));
    }

    @Test
    public void solutionTestC() {
        final ProcessorSchedulingSolution<Integer> solution = problemC().solve(Algorithms.HOCHBAUM_SHMOYS);
        assertTheorem1(solution, 165);
    }

    @Test
    public void solutionTestD() {
        final ProcessorSchedulingSolution<Integer> solution = problemD().solve(Algorithms.HOCHBAUM_SHMOYS);
        assertTheorem1(solution, 90);
    }

    private void assertTheorem1(ProcessorSchedulingSolution solution, int minimalMakespan) {
        assertTrue(solution.getMakespan() <= (1 + 1.0/5.0)*(1 + Math.pow(2, -HochbaumShmoysApproximation.DEFAULT_NUM_ITERATIONS))*minimalMakespan); // checks that theorem 1 from Hochbaum & Shmoys is satisfied
    }

}
