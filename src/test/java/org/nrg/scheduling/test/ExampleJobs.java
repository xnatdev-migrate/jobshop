package org.nrg.scheduling.test;

import org.nrg.scheduling.Job;
import org.nrg.scheduling.JobList;
import org.nrg.scheduling.ProcessorSchedulingProblem;

public class ExampleJobs {

    public static final Job<String> shortJob = new Job<>("short", 100);
    public static final Job<String> mediumJob = new Job<>("medium", 500);
    public static final Job<String> longJob = new Job<>("long", 1000);
    public static final Job<String> reallyLongJob = new Job<>("really long", 20000);

    public static ProcessorSchedulingProblem<String> problemA() {
        final Job<String> fs = new Job<>("FS", 840);
        final Job<String> manualPup = new Job<>("Manual PUP", 120);
        final Job<String> fsPup = new Job<>("FS PUP", 150);
        final Job<String> fBIRN = new Job<>("FBIRN", 15);
        final Job<String> bold = new Job<>("BOLD", 90);
        final Job<String> registerMR = new Job<>("registerMR", 10);
        final Job<String> wmh = new Job<>("WMH", 20);

        final JobList<String> jobList = new JobList<>(wmh, registerMR, bold, fBIRN, fsPup, manualPup, fs);
        return new ProcessorSchedulingProblem<>(jobList, 4);
    }

    public static ProcessorSchedulingProblem<Integer> problemB() {
        final Job<Integer> job1 = new Job<>(1, 1000000);
        final Job<Integer> job2 = new Job<>(2, 10);
        final Job<Integer> job3 = new Job<>(3, 1);
        final JobList<Integer> jobList = new JobList<>(job1, job2, job3);
        return new ProcessorSchedulingProblem<>(jobList, 3);
    }

    public static ProcessorSchedulingProblem<Integer> problemC() {
        final Job<Integer> job1 = new Job<>(1, 100);
        final Job<Integer> job2 = new Job<>(2, 70);
        final Job<Integer> job3 = new Job<>(3, 45);
        final Job<Integer> job4 = new Job<>(4, 10);
        final Job<Integer> job5 = new Job<>(5, 75);
        final Job<Integer> job6 = new Job<>(6, 80);
        final Job<Integer> job7 = new Job<>(7, 50);
        final Job<Integer> job8 = new Job<>(8, 35);
        final Job<Integer> job9 = new Job<>(9, 20);
        final Job<Integer> job10 = new Job<>(10, 3);
        final Job<Integer> job11 = new Job<>(11, 7);

        return new ProcessorSchedulingProblem<>(new JobList<>(job1, job2, job3, job4, job5, job6, job7, job8, job9, job10, job11), 3); // exists perfectly efficient machine allocation where all 3 machines have run time 165
    }

    public static ProcessorSchedulingProblem<Integer> problemD() {
        final Job<Integer> job1 = new Job<>(1, 10);
        final Job<Integer> job2 = new Job<>(2, 15);
        final Job<Integer> job3 = new Job<>(3, 15);
        final Job<Integer> job4 = new Job<>(4, 50);
        final Job<Integer> job5 = new Job<>(5, 10);
        final Job<Integer> job6 = new Job<>(6, 17);
        final Job<Integer> job7 = new Job<>(7, 23);
        final Job<Integer> job8 = new Job<>(8, 31);
        final Job<Integer> job9 = new Job<>(9, 9);
        final JobList<Integer> jobList = new JobList<>(job1, job2, job3, job4, job5, job6, job7, job8, job9);

        for (int i = 10; i < (10 + 18); i ++) {
            jobList.add(new Job<>(i, 5));
        }

        for (int i = 28; i < (28 + 45); i ++) {
            jobList.add(new Job<>(i, 2));
        }

        return new ProcessorSchedulingProblem<>(jobList, 4); // exists perfectly efficient machine allocation where all 4 machines have run time 90
    }

}
