param n, integer, > 0;
param k, > 0;

set J := 0..n-1;
/* set of items */
set M := 0..k-1;
/* set of bins */

param T{i in J}, > 0;

var x{i in J, j in M}, binary;
var t >= 0;

minimize obj: t;

s.t. assignToOneProcessor{i in J}: sum{j in M} x[i,j] = 1;
/* each job must be allocated to exactly one processor */
s.t. makespanGeqEachMachine{j in M}: sum{i in J} T[i] * x[i,j] <= t;
/* Makespan is at least as large as any machine's runtime */

end;