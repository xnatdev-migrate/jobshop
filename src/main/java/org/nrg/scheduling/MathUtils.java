package org.nrg.scheduling;

public class MathUtils {

    public static final double EPSILON = 0.00000001;

    public static boolean doublesEqual(double d1, double d2) {
        return Math.abs(d1 - d2) < EPSILON;
    }

    public static int differenceToIntComparison(double difference) {
        if (difference > EPSILON) return -1;
        else if (difference < -EPSILON) return 1;
        else return 0;
    }

}
