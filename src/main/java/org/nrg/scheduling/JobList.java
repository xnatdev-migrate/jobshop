package org.nrg.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.lang.Math;

public class JobList<X> extends ArrayList<Job<X>> {

    @SafeVarargs
    public JobList(Job<X>... elements) {
        for (Job<X> element : elements) {
            add(element);
        }
    }

    public static <X> JobList<X> singleton(Job<X> job) {
        final JobList<X> list = new JobList<>();
        list.add(job);
        return list;
    }

    public void sort() {
        Collections.sort(this);
    }

    public Job<X> getLastJob() {
        return get(size() - 1);
    }

    public double maxJobsize() {
        return Collections.min(this).getJobLength();
    }

    public List<Double> jobSizes() {
        List<Double> jobSizes = new ArrayList<>();
        for (Job job : this) {
            jobSizes.add(job.getJobLength());
        }
        return jobSizes;
    }

    public double totalJobLength() {
        double jobSum = 0;
        for (Job job : this) {
            jobSum += job.getJobLength();
        }
        return jobSum;
    }

    public double calculateSize(int numMachines) {
        return Math.max(totalJobLength()/numMachines, maxJobsize());
    }

    public JobList<X> cherryPick(int... indices) {
        final JobList<X> cherryPicked = new JobList<>();
        for (int index : indices) {
            cherryPicked.add(get(index));
        }
        cherryPicked.sort();
        return cherryPicked;
    }

    public JobList<X> deepCopy() {
        return subList(0, size());
    }

    @Override
    public JobList<X> subList(int fromIndex, int toIndex) {
        final JobList<X> list = new JobList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(get(i));
        }
        return list;
    }

    /**
     * Scales all job times by some scaling factor, returning a deep copy of this
     * @param scalingFactor factor to rescale by
     * @return deep-copied rescaled list
     */
    public JobList<X> rescaleJobs(double scalingFactor) {
        final JobList<X> newJobList = new JobList<>();
        for (Job<X> job : this) {
            newJobList.add(new Job<>(job.getJobId(), job.getJobLength()/scalingFactor));
        }
        return newJobList;
    }
}
