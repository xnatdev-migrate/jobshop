package org.nrg.scheduling;

public class ProcessorSchedulingProblem<X> {

    private JobList<X> jobList;
    private int numMachines; // Variable "m" from Hochbaum and Shmoys

    public ProcessorSchedulingProblem(JobList<X> jobList, int numMachines) {
        this.jobList = jobList;
        this.numMachines = numMachines;
    }

    public JobList<X> getJobList() {
        return jobList;
    }

    public int getNumMachines() {
        return numMachines;
    }

    /*
    This method attenots to apply the reduction attached as a PDF under resources: if one job is very expensive, we can just give it its own machine.
    I'm guessing this is a theorem that someone already proved out there, but I never found it when going over papers, so I don't know who to credit.
    After reduction is applied (possibly several times), solves the problem using a standard algorithm for the problem.
     */
    public ProcessorSchedulingSolution<X> solve(ProcessorSchedulingAlgorithm algorithm) {
        final ProcessorSchedulingAlgorithm usedAlgorithm = (algorithm == null) ? Algorithms.HOCHBAUM_SHMOYS : algorithm;
        jobList.sort();
        if (jobList.maxJobsize() >= jobList.totalJobLength()/numMachines && numMachines > 1) { // *reduction*
            JobList<X> otherJobs = jobList.subList(1, jobList.size());
            ProcessorSchedulingProblem<X> subProblem = new ProcessorSchedulingProblem<>(otherJobs, numMachines - 1);
            final ProcessorSchedulingSolution<X> subProblemSolution = subProblem.solve(usedAlgorithm);
            subProblemSolution.addSingleMachineJob(jobList.get(0));
            subProblemSolution.sortEachList();
            subProblemSolution.processMeasures();
            return subProblemSolution;
        } else {
            ProcessorSchedulingSolution<X> solution = usedAlgorithm.solve(this);
            solution.sortEachList();
            solution.processMeasures();
            return solution;
        }
    }

}
