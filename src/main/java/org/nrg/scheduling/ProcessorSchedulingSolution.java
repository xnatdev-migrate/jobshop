package org.nrg.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProcessorSchedulingSolution<X> {

    private List<JobList<X>> machineAllocations;
    private JobList<X> queueingOrder;
    private JobList<X> completionOrder;

    public void addSingleMachineJob(Job<X> job) {
        machineAllocations.add(0, JobList.singleton(job));
        processMeasures();
    }

    public ProcessorSchedulingSolution(int numMachines) {
        machineAllocations = new ArrayList<>();
        for (int i = 0; i < numMachines; i++) {
            machineAllocations.add(new JobList<X>());
        }
    }

    public ProcessorSchedulingSolution(List<JobList<X>> machineAllocations) {
        this.machineAllocations = machineAllocations;
    }

    // machine number starts at 0
    public void assignJob(Job<X> job, int machineNumber) {
        machineAllocations.get(machineNumber).add(job);
    }

    public void restoreOriginalTimes(JobList<X> originalJobList) {
        for (JobList<X> jobList : machineAllocations) {
            for (Job<X> job : jobList) {
                job.setJobLength(findJob(job.getJobId(), originalJobList).getJobLength());
            }
        }
    }

    public void sortEachList() {
        for (JobList<X> list : machineAllocations) {
            list.sort();
        }
    }

    public List<JobList<X>> getSolutionByMachineAllocation() {
        return machineAllocations;
    }

    public JobList<X> getSolutionByQueueOrder() {
        return queueingOrder;
    }

    public JobList<X> getSolutionByCompletionOrder() {
        return completionOrder;
    }

    public double getMakespan() {
        return completionOrder.getLastJob().getEndTime();
    }

    public void processMeasures() {
        final List<JobList<X>> copy = new ArrayList<>();
        for (JobList<X> list : machineAllocations) {
            copy.add(list.deepCopy());
        }

        queueingOrder = new JobList<>();
        completionOrder = new JobList<>();

        for (JobList<X> jobList : copy) {
            double cumulativeTime = 0;
            for (Job job : jobList) {
                job.setTimes(cumulativeTime);
                cumulativeTime += job.getJobLength();
            }
        }

        for (JobList<X> jobList : copy) {
            queueingOrder.add(jobList.get(0)); // launch the first job in every queue first
            completionOrder.add(jobList.get(0)); // sorted later (order not determined by this insertion)
            jobList.remove(0);
        }

        final JobList<X> remainingJobs = new JobList<>();
        for (JobList<X> jobList : copy) {
            remainingJobs.addAll(jobList);
            completionOrder.addAll(jobList);
        }

        Collections.sort(remainingJobs, new Comparator<Job<X>>() {
            @Override
            public int compare(Job<X> o1, Job<X> o2) {
                return o2.startComparison(o1);
            }
        });

        queueingOrder.addAll(remainingJobs);
        Collections.sort(completionOrder, new Comparator<Job<X>>() {
            @Override
            public int compare(Job<X> o1, Job<X> o2) {
                return o2.finishComparison(o1);
            }
        });
    }

    public Job<X> findJob(X jobId, JobList<X> originalJobList) {
        for (Job<X> job : originalJobList) {
            if (jobId.equals(job.getJobId())) return job;
        }
        throw new RuntimeException(String.format("Job with ID %s not found.", jobId));
    }

}
