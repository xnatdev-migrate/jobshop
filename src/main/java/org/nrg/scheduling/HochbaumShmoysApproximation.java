package org.nrg.scheduling;

import java.util.List;

public class HochbaumShmoysApproximation extends ProcessorSchedulingAlgorithm {

    public static final int DEFAULT_NUM_ITERATIONS = 5;
    private int numIterations = DEFAULT_NUM_ITERATIONS; // Variable "k" from Hochbaum and Shmoys

    public HochbaumShmoysApproximation(int numIterations) {
        this.numIterations = numIterations;
    }

    public HochbaumShmoysApproximation() {}

    @Override
    public <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem) {
        final JobList<X> jobList = problem.getJobList();
        final int numMachines = problem.getNumMachines();

        jobList.sort();
        List<JobList<X>> binPackingSolution;
        final double SIZE = jobList.calculateSize(numMachines);
        double upper = 2*SIZE;
        double lower = SIZE;
        double scalingFactor; // Variable "d" from the paper
        for (int i = 0; i < numIterations; i++) {
            scalingFactor = (upper + lower)/2; //
            binPackingSolution = new BinPackingProblem<>(scalingFactor, jobList).solveBinPackingApproximation();
            if (binPackingSolution.size() > numMachines) {
                lower = scalingFactor;
            } else {
                upper = scalingFactor;
            }
        }
        ProcessorSchedulingSolution<X> jobShopSolution = new ProcessorSchedulingSolution<>(new BinPackingProblem<>(upper, jobList).solveBinPackingApproximation());
        jobShopSolution.restoreOriginalTimes(jobList);
        return jobShopSolution;
    }

}
