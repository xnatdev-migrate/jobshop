package org.nrg.scheduling;

public class Job<X> implements Comparable<Job> {

    private X jobId;
    private double jobLength;
    private double launchTime;
    private double endTime;

    public Job(X jobId, double jobLength) {
        this.jobId = jobId;
        this.jobLength = jobLength;
    }

    public X getJobId() {
        return jobId;
    }

    public void setJobLength(double jobLength) {
        this.jobLength = jobLength;
    }

    public double getJobLength() {
        return jobLength;
    }

    public double getLaunchTime() {
        return launchTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setTimes(double cumulativeMachineTime) {
        launchTime = cumulativeMachineTime;
        endTime = launchTime + jobLength;
    }

    public int compareTo(Job comparedJob) {
        return differenceToIntFlag(jobLength - comparedJob.getJobLength());
    }

    public int startComparison(Job comparedJob) {
        return differenceToIntFlag(launchTime - comparedJob.getLaunchTime());
    }

    public int finishComparison(Job comparedJob) {
        return differenceToIntFlag(endTime - comparedJob.getEndTime());
    }

    private int differenceToIntFlag(double difference) {
        return MathUtils.differenceToIntComparison(difference);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Job job = (Job) o;

        return jobId.equals(job.jobId) && jobLength == job.jobLength;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = jobId != null ? jobId.hashCode() : 0;
        temp = Double.doubleToLongBits(jobLength);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Job: " + jobId.toString();
    }
}
