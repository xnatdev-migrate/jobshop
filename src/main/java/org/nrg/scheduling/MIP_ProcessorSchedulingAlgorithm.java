package org.nrg.scheduling;

import com.google.common.base.Joiner;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class MIP_ProcessorSchedulingAlgorithm extends ProcessorSchedulingAlgorithm {

    @Override
    public <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem) {
        final JobList<X> jobList = problem.getJobList();
        final int numMachines = problem.getNumMachines();
        Path temp;
        final String problemStatement = "processorScheduling.mod";
        final String data = "data.dat";
        final String output = "output.txt";

        try {
            temp = Files.createTempDirectory("processorScheduling");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final Path destination = temp.resolve(problemStatement);
        try {
            File file = new File(Thread.currentThread().getContextClassLoader().getResource(problemStatement).toURI());
            Files.copy(file.toPath(), destination);
        } catch (Exception e) {
            try {
                Files.copy(MIP_ProcessorSchedulingAlgorithm.class.getClassLoader().getResourceAsStream(problemStatement), destination);
            } catch (Exception e2) {
                throw new RuntimeException("Failed to read file for problem statement representation", e2);
            }
        }

        try {
            final FileWriter writer = new FileWriter(temp.resolve(data).toFile());
            writer.write(String.format("param k := %d;\n\n", numMachines));
            writer.write(String.format("param n := %d;\n\n", jobList.size()));

            List<String> dataValues = new ArrayList<>();
            for (int i = 0; i < jobList.size(); i++) {
                dataValues.add(String.format("%d %d", i, (int)jobList.get(i).getJobLength()));
            }
            writer.write(String.format("param T := %s;\n\n", Joiner.on(", ").join(dataValues)));

            writer.write("end;\n");
            writer.close();
        } catch (Exception e) {
            throw new RuntimeException("Failed to construct data file to solve scheduling problem.", e);
        }

        try {
            final Process glpsol;
            final ProcessBuilder processBuilder = new ProcessBuilder("glpsol", "-m", problemStatement, "-d", data, "-w", output);
            processBuilder.directory(temp.toFile());
            glpsol = processBuilder.start();
            glpsol.waitFor();
        } catch (Exception e) {
            throw new RuntimeException("Failed to solve problem via GLPK.", e);
        }

        final List<String> outputLines;
        try {
            outputLines = Files.readAllLines(temp.resolve(output), Charset.defaultCharset());
        } catch (Exception e) {
            throw new RuntimeException("Failed to read in GLPK solution from file");
        }

        final List<String> solutionLines = new ArrayList<>();
        for (String line : outputLines) {
            if (line.startsWith("j")) solutionLines.add(line);
        }
        solutionLines.remove(solutionLines.size() - 1); // drop the line corresponding to the makespan

        final ProcessorSchedulingSolution<X> solution = new ProcessorSchedulingSolution<>(numMachines);
        for (int i = 0; i < solutionLines.size(); i++) {
            String[] parts = solutionLines.get(i).split(" ");
            if (Integer.parseInt(parts[2]) == 1) {
                solution.assignJob(jobList.get(i/numMachines), i % numMachines);
            }
        }
        return solution;
    }

}
