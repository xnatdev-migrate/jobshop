package org.nrg.scheduling;

public abstract class ProcessorSchedulingAlgorithm {

    public abstract <X> ProcessorSchedulingSolution<X> solve(ProcessorSchedulingProblem<X> problem);

}
