package org.nrg.scheduling;

public class Algorithms {

    public static ProcessorSchedulingAlgorithm HOCHBAUM_SHMOYS = new HochbaumShmoysApproximation();
    public static ProcessorSchedulingAlgorithm MIP = new MIP_ProcessorSchedulingAlgorithm();

    public static ProcessorSchedulingAlgorithm hochbaumShmoys(int numIterations) {
        return new HochbaumShmoysApproximation(numIterations);
    }

}
