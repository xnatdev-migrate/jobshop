package org.nrg.scheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinPackingProblem<X> {

    private JobList<X> jobList; // assumes jobList are already sorted

    public BinPackingProblem(double scalingFactor, JobList<X> jobList) {
        this.jobList = jobList.rescaleJobs(scalingFactor);
    }

    public List<JobList<X>> solveBinPackingApproximation() {
        JobList<X> remainingJobs, smallJobs;
        int indexCutoff = Collections.binarySearch(jobList, new Job<X>(null, 0.2), null);
        if (indexCutoff < 0) indexCutoff = -indexCutoff - 1;
        remainingJobs = jobList.subList(0, indexCutoff);
        smallJobs = jobList.subList(indexCutoff, jobList.size());

        List<JobList<X>> packedBins = new ArrayList<>();

        while (remainingJobs.size() > 0) {
            Job<X> piece = remainingJobs.get(0);

            if (remainingJobs.size() == 1) {
                // if there's only 1 piece left to pack
                packedBins.add(remainingJobs.cherryPick(0));
                remainingJobs.remove(0);
            }

            else {
                double pieceSize = piece.getJobLength();
                if (pieceSize >= 0.6) {
                    // Stage 1
                    List<Integer> smallPieceSingleton = largestPieceIndices(remainingJobs, 1 - pieceSize);
                    if (smallPieceSingleton == null) packedBins.add(JobList.singleton(piece)); // if we can't
                    else {
                        int smallJobIndex = smallPieceSingleton.get(0);
                        packedBins.add(remainingJobs.cherryPick(0, smallJobIndex));
                        remainingJobs.remove(smallJobIndex);
                    }
                    remainingJobs.remove(0);
                } else if (remainingJobs.size() == 2) {
                    packedBins.add(remainingJobs.cherryPick(0, 1)); // if there's only 2 pieces less than 0.6, pack em and call it a day. Can assume now that remainingJobs.size() >= 3
                    remainingJobs.remove(1);
                    remainingJobs.remove(0);
                } else if (pieceSize >= 0.5 && pieceSize < 0.6) {
                    // Stage 2
                    if (remainingJobs.get(1).getJobLength() >= 0.5) {
                        // easy case in Stage 2:
                        packedBins.add(remainingJobs.cherryPick(0, 1));
                        remainingJobs.remove(1);
                        remainingJobs.remove(0);
                    } else {
                        // guess version of Stage 2
                        double twoSize = remainingJobs.get(1).getJobLength();
                        List<Integer> threeBinCompanions = largestPieceIndices(remainingJobs, 0.3, 0.2);
                        double threeSize = (threeBinCompanions == null) ? 0 : remainingJobs.get(threeBinCompanions.get(0)).getJobLength() + remainingJobs.get(threeBinCompanions.get(1)).getJobLength();
                        if (twoSize >= threeSize) {
                            // pack it in a 2-bin
                            packedBins.add(remainingJobs.cherryPick(0, 1));
                            remainingJobs.remove(1);
                            remainingJobs.remove(0);
                        } else {
                            // pack it in a 3-bin
                            packedBins.add(remainingJobs.cherryPick(0, threeBinCompanions.get(0), threeBinCompanions.get(1)));
                            remainingJobs.remove(threeBinCompanions.get(1).intValue());
                            remainingJobs.remove(threeBinCompanions.get(0).intValue());
                            remainingJobs.remove(0);
                        }
                    }
                } else if (pieceSize >= 0.4) { // already know there are three or more pieces, and all pieces are less than 0.5. So apply Stage 3
                    List<Integer> threeBinIndices = largestPieceIndices(remainingJobs, 0.5, 0.4, 0.3);
                    if (threeBinIndices == null) { // Stage 4 embedded here. If all pieces were too big for the above search to work
                        packedBins.add(remainingJobs.cherryPick(0, 1));
                        remainingJobs.remove(1);
                        remainingJobs.remove(0);
                    } else {
                        packedBins.add(remainingJobs.cherryPick(threeBinIndices.get(0), threeBinIndices.get(1), threeBinIndices.get(2)));
                        remainingJobs.remove(threeBinIndices.get(2).intValue());
                        remainingJobs.remove(threeBinIndices.get(1).intValue());
                        remainingJobs.remove(0);
                    }
                } else {
                    // Stage 4 is taken care of by earlier checks. Skip it here. Do stage 5
                    double smallestPiece = remainingJobs.get(remainingJobs.size() - 1).getJobLength();
                    List<Integer> fourBin = null;
                    if (smallestPiece > 0.25) {
                        double delta = 0.25 - smallestPiece;
                        fourBin = largestPieceIndices(remainingJobs, 0.25 + 3*delta, 0.25 + delta, 0.25 + delta/3, smallestPiece + 0.00000001); // we want to get the smallest piece here too
                    }

                    if (smallestPiece > 0.25 || fourBin == null) {
                        while (remainingJobs.size() > 0) {
                            final int binSize = Math.min(3, remainingJobs.size());
                            packedBins.add(remainingJobs.subList(0, binSize));
                            for (int i = 0; i < binSize; i++) {
                                remainingJobs.remove(0);
                            }
                        }
                        break;
                    } else {
                        packedBins.add(remainingJobs.cherryPick(fourBin.get(0), fourBin.get(1), fourBin.get(2), fourBin.get(3)));
                        remainingJobs.remove(fourBin.get(3).intValue());
                        remainingJobs.remove(fourBin.get(2).intValue());
                        remainingJobs.remove(fourBin.get(1).intValue());
                        remainingJobs.remove(fourBin.get(0).intValue());
                    }
                }
            }
        }

        for (Job<X> job : smallJobs) {
            JobList<X> leastFilledBin = packedBins.get(0);

            for (JobList<X> bin : packedBins) {
                if (bin.totalJobLength() < leastFilledBin.totalJobLength()) {
                    leastFilledBin = bin;
                }
            }

            if (leastFilledBin.totalJobLength() > 1) leastFilledBin = null;

            if (leastFilledBin == null) {
                // all bins have at least 1
                packedBins.add(JobList.singleton(job));
            } else {
                leastFilledBin.add(job);
            }
        }

        return packedBins;
    }

    private List<Integer> largestPieceIndices(JobList<X> jobs, double... maxima) {
        // computes the indices in the list for L[u_k, ..., u_1]
        // returns null iff L[u_k, ..., u_1] does not exist
        // parameters are reversed order from the paper
        try {
            List<Integer> indices = new ArrayList<>();

            for (int maxIndex = 0; maxIndex < maxima.length; maxIndex++) {
                int index = Collections.binarySearch(jobs, new Job<>(null, maxima[maxIndex]), null);
                if (index >= 0) {
                    // highly unlikely, it means we got a perfect match
                    if (indices.contains(index)) indices.add(index + 1); // if we're already using this one, get the next smallest
                } else {
                    index = -index - 1;
                    if (indices.contains(index) || jobs.get(index).getJobLength() > maxima[maxIndex]) throw new ArrayIndexOutOfBoundsException("Couldn't calculate piece indices");
                    indices.add(index);
                }
            }
            return indices;
        } catch (Exception ignored) {
            // If at any point we hit this, the pieces did not exist
            return null;
        }
    }


}
